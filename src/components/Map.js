
// import 'babel-polyfill';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import mapboxgl from 'mapbox-gl';
// let capitals=require('/custom.geo.json');
import capitals from '../../public/countries.json'
import countries from '../../public/custom.geo.json'
import axios from 'axios';
import ReactMapGL, {Popup} from 'react-map-gl';

mapboxgl.accessToken = 'pk.eyJ1Ijoiam9zZWluZm9ybWF0aWNvMjAxNSIsImEiOiJja2JiZ2FoenEwMTAwMnptZjZ1YTBoMDA2In0.3ly_xnQH4Wmr1OluwS5ZCQ';


export default class Map extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lng: -62,
            lat: -25,
            zoom: 3,
            text:"",
            capitals:capitals,
            countries:countries,

        };
        
        this.ref= React.createRef();

    }

        async apiCall(city) {
            console.log("City: "+city);
            // FIRST ATTEMPT FROM REDIS
            let redisResp =await axios.post("/redisApi",{params: {city:city}});
            console.log("repuesta de redis: "+JSON.stringify(redisResp.data));
            if(redisResp.data.message=="No city found in redis"){
                const res= await axios.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=d607e8162ca5cd971bc0b2fe11cb2e72&units=metric");
                console.log("respuesta de openwheater: " +res.data.main.temp);
                await axios.post("/saveRedis",{params: {city:city,temp:res.data.main.temp}});
                return res.data.main.temp;
            }else{
                console.log("redis responce city temp: " +JSON.stringify(redisResp.data.temp));
                return redisResp.data.temp;
            }
        }

        componentDidMount(){
            const map = new mapboxgl.Map({
                container: this.mapContainer,
                style: 'mapbox://styles/mapbox/streets-v11',
                center: [this.state.lng, this.state.lat],
                zoom: this.state.zoom
            });
            var hoveredStateId = null;
            
            map.on('load', () =>{

                // Add a source for the state polygons.
                map.addSource('states', {
                'type': 'geojson',
                'data':countries
                });
                // Add a layer showing the state polygons.
                map.addLayer({
                'id': 'states-layer',
                'type': 'fill',
                'source': 'states',
                'paint': {
                // 'fill-color': 'rgba(200, 100, 240, 0.4)',
                // 'fill-outline-color': 'rgba(200, 100, 240, 1)'

                'fill-color': 'rgba(200, 100, 240, 0.4)',
                'fill-outline-color': 'rgba(200, 100, 240, 1)',
                'fill-opacity': 0
                }
                });

                map.addLayer({
                    'id': 'lineas',
                    'type': 'line',
                    'source': 'states',
                    'paint': {
                    // 'fill-color': 'rgba(200, 100, 240, 0.4)',
                    // 'fill-outline-color': 'rgba(200, 100, 240, 1)'
                        'line-color': 'rgba(200, 100, 240, 0.4)',
                        'line-width': 2,
                
                    }
                    });

            });
                
            map.on('click', 'states-layer', async (e) =>{
                console.log(e.features[0].properties.name);
                let pais=e.features[0].properties.name;
                let res=capitals.find((country)=>country.name === e.features[0].properties.name);
                // console.log(res);
                let temp=await this.apiCall(res.capital);
                let capital=res.capital;
                console.log(temp);
                new mapboxgl.Popup({ closeOnClick: true,closeButton: false })
                .setLngLat(e.lngLat)
                // .setHTML(e.features[0].properties.name)
                .setHTML("<div>"+pais+","+capital+"</div><div style='text-align: center'>"+temp+"°</div>")
                .addTo(map);
            });

            map.scrollZoom.disable();
            map.dragRotate.disable();
            map.touchZoomRotate.disableRotation();
            map.boxZoom.disable();
            // map.dragPan.disable();
            map.dragRotate.disable();
            map.keyboard.disable();
            map.doubleClickZoom.disable();
            map.touchZoomRotate.disable();
        }    
            render() {
                
                console.log(countries, capitals);
                return (
                  <div>
                  {/* <div ref={el => this.mapContainer = el} className="mapContainer" /> */}
                  <div ref={el => this.mapContainer = el} id="map" />
                  </div>
                )
            }
}
