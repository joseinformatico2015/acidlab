import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Map from './Map'

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={Map}/>
                </Switch>
            </BrowserRouter>
        )
    }
}
export default App