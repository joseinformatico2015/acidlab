
const express= require('express');
const redis= require('redis');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const axios = require('axios');
const config = require('../configs/config');
const cors = require('cors');
const fetch = require("node-fetch");
const path = require('path');

const app= express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use(express.static(path.join(__dirname,"..","dist")))
// app.use(express.static(path.join(__dirname,"..","public")))

app.set('master_key', config.llave);
const client = redis.createClient({
    host: 'redis-server',
    port: 6379
})

client.on('connect', ()=>{
    console.log('Coneccion a redis exitosa');
})

client.on('error', function(err) {
    console.log('Redis error: ' + err);
});


app.post('/redisApi',(req, res)=>{
    console.log("en redis api buscando: "+req.body.params.city);
    let city=req.body.params.city;
    // GET DATA FROM REDIS CACHE
    client.hgetall(city,function(err,resp){
        if(err){
            console.log("consulta a redis error: "+err);
            res.json({ message: "error" });
        }else{
            if(resp==null){
                console.log("respuesta de redis NULL: ");
                res.json({ message: "No city found in redis" });
            }else{  
                    console.log("respuesta de redis: "+resp.temp);
                    res.json({
                        message: 'City Found',
                        temp: resp.temp
                    });
            }
        }
    })
});

app.post('/saveRedis',async (req, res)=>{
    let city=req.body.params.city;
    let temp=req.body.params.temp;
    let create_at=Date.now();
    // const resp= await axios.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=d607e8162ca5cd971bc0b2fe11cb2e72&units=metric");
    // console.log("respuesta api externa: "+resp.data.main.temp);
   
    // SAVE DATA IN CACHE REDIS
    client.hmset(city,["temp",temp,"create_at", create_at],function(err,resp){
        if(err){
            console.log(err);
            res.json({ message: "error save data in redis" });
        }else{
            console.log(city+"city temp save in redis");
            res.json({  message: "city temp save in redis"});
        }
    })

    // console.log("mandando a react: "+JSON.stringify(data.main.temp));
    // return res.json({ temp: data.main.temp });
})
   
module.exports =  app;